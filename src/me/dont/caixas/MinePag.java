package me.dont.caixas;

import org.bukkit.entity.Player;



public class MinePag {

	static boolean to = false;
	
	public static void setShop(){
        if (Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("MinePag") != null) {
        	to = true;
            Main.economia = Economy.MINEPAG;
        }
	}
	
	public static int getMoney(Player p){
		if (to){
		return (int) com.minepag.cash.Cash.getCash(p.getName());
		}
		return 0;
	}
	
	public static void removeMoney(Player p, int quant){
		if (to){
		com.minepag.cash.Cash.setCash(p.getName(), getMoney(p)-quant);
		}
	}
	
}
