package me.dont.caixas;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class DarCaixa implements CommandExecutor, TabCompleter{

	
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String arg2, String[] args) {
		if (command.getName().equalsIgnoreCase("darcaixa")){
			if (sender.hasPermission("dont.darcaixas")){
				if (args.length==0){
					sender.sendMessage("�cSintaxe incorreta. (/darcaixa <player> <tipo> <quantia>");
				}
				else if (args.length==3){
					if (args[0].equalsIgnoreCase("*")){
						if (sender.hasPermission("dont.darcaixas.all")){
							for (Player player : Bukkit.getOnlinePlayers()){
								if (Main.caixas.containsKey(args[1])){
									player.getInventory().addItem(Main.caixas.get(args[1]).getBox(Integer.valueOf(args[2])));
									sender.sendMessage("�cCaixa dado com sucesso");
								}
								else {
									sender.sendMessage("�cNenhuma caixa com esse identificador");
									sender.sendMessage("�cIdentificador v�lidos: "+Main.caixas.keySet());
								}
							}
						}

					}
					else {
					Player player = Bukkit.getPlayer(args[0]);
					if (player == null){
						sender.sendMessage("�cEste player n�o est� online");
					}
					else {
						if (Main.caixas.containsKey(args[1])){
							player.getInventory().addItem(Main.caixas.get(args[1]).getBox(Integer.valueOf(args[2])));
							sender.sendMessage("�cCaixa dado com sucesso");
						}
						else {
							sender.sendMessage("�cNenhuma caixa com esse identificador");
							sender.sendMessage("�cIdentificador v�lidos: "+Main.caixas.keySet());
						}
					}
				}
				}
				else {
					sender.sendMessage("�cSintaxe incorreta. (/darcaixa <player> <tipo> <quantia>");
				}
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String arg2, String[] args) {
		if (command.getName().equalsIgnoreCase("darcaixa") && args.length >=2){
			List<String> lista = new ArrayList<>();
			for (String s : Main.caixas.keySet()){
				lista.add(s);
			}
			return lista;
		}
		return null;
	}

}
