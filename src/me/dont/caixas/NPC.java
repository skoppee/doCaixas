package me.dont.caixas;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class NPC implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String arg2, String[] arg3) {
		if (command.getName().equalsIgnoreCase("setcaixanpc") && sender.hasPermission("dont.setnpc")){
			if (sender instanceof Player){
				Player p = (Player)sender;
			Villager npc = (Villager) p.getWorld().spawnEntity(new Location(p.getWorld(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), p.getLocation().getYaw(), p.getLocation().getPitch()), EntityType.VILLAGER);
				npc.setCustomName(Main.npc);
			npc.setCustomNameVisible(true);
			npc.setProfession(Profession.valueOf(Main.profissao));
			RemoveAI.disableAI(npc);
			}
			else {
				sender.sendMessage("�cComando n�o suportado para console/command block");
			}
		}
		return false;
	}
	
	@EventHandler
	public void onNPC(PlayerInteractEntityEvent e){
		if (e.getRightClicked().getType().equals(EntityType.VILLAGER) && e.getRightClicked().getCustomName().equalsIgnoreCase(Main.npc)){
			e.setCancelled(true);
			GUIs.MenuPrincipal(e.getPlayer());
		}
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if (e.getEntity().getType().equals(EntityType.VILLAGER) && e.getEntity().getCustomName().equalsIgnoreCase(Main.npc)){
			if (e.getDamager() instanceof Player){
				if (e.getDamager().hasPermission("dont.setnpc")){
					Player p = (Player) e.getDamager();
					if (p.getItemInHand().getType().equals(Material.GOLD_SWORD)){
						e.getEntity().remove();
					}
					else {
						e.setCancelled(true);
						p.sendMessage("�cMate o npc com uma espada de ouro");
					}
				}
				else {
					e.setCancelled(true);
				}
			}
			else {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent e){
		if (e.getCurrentItem() == null || e.getSlotType() == SlotType.OUTSIDE) return;
		if (e.getInventory().getName() == Main.nome){
			e.setCancelled(true);
			Caixa caixa = null;
			boolean to = false;
			Player p = (Player) e.getWhoClicked();
			for (Caixa c : Main.caixas.values()){
				if (e.getSlot() == c.getSlot()){
					caixa = c;
					to = true;
				}
			}
			if (to){
				if (e.isLeftClick()){
					AbrirCaixa.previewBox(p, caixa);
				}
				else {
					if (!Main.buy) return;
					p.closeInventory();
					if (Economia.getMoney(p) >= caixa.getPreco()){
						Economia.removeBalance(p, caixa.getPreco());
						p.getInventory().addItem(caixa.getBox());
						p.sendMessage("�6Voc� comprou uma caixa misteriosa com sucesso");
					}
					else {
						p.sendMessage("�cVoc� n�o tem dinheiro suficiente");
					}
				}
			}
		}
	}
	
}
