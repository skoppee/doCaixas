package me.dont.caixas;

import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Vault {

    public static net.milkbowl.vault.economy.Economy econ = null;
    
    public static boolean setupEconomy() {
        if (Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<net.milkbowl.vault.economy.Economy> rsp = Main.getPlugin(Main.class).getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        Main.economia = Economy.VAULT;
        return econ != null;
    }
    
    public static int getMoney(Player p){
    	return (int) econ.getBalance(p);
    }
    
    public static void removeBalance(Player p, int quant){
    	econ.withdrawPlayer(p, quant);
    }
	
}
