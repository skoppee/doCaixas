package me.dont.caixas;

import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class GUIs {

	public static void MenuPrincipal(Player p){
		Inventory inv = Bukkit.createInventory(null, Main.tamanho, Main.nome);
		if (Main.buy){
		for (Entry<String, Caixa> aa: Main.caixas.entrySet()){
			inv.setItem(aa.getValue().getSlot(), aa.getValue().getBoxToMenu());
		}
		}
		else {
			for (Entry<String, Caixa> aa: Main.caixas.entrySet()){
				inv.setItem(aa.getValue().getSlot(), aa.getValue().getBox(1));
			}
		}
		
		p.openInventory(inv);
	}
	
}
