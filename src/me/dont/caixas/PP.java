package me.dont.caixas;

import org.black_ixx.playerpoints.PlayerPoints;
import org.black_ixx.playerpoints.PlayerPointsAPI;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class PP {

	public static PlayerPointsAPI ppapi = null;
	public static PlayerPoints play = null;
	
	public static void setPlayerPoints(){
        if (Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("PlayerPoints") != null) {
            Plugin plugin = Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("PlayerPoints");
	        Main.economia = Economy.PLAYERPOINTS;

            play = PlayerPoints.class.cast(plugin);
        	ppapi = new PlayerPointsAPI(play);
        }
	}
	
	public static int getMoney(Player p){
		return ppapi.look(p.getName());
	}
	
	public static void removeBalance(Player p, int quant){
		ppapi.set(p.getName(), getMoney(p)-quant);
	}
	
}
