package me.dont.caixas;

import org.bukkit.entity.Player;

import com.mineshop.api.MineSHOP;

public class MineShop {

	private static MineSHOP shop = null;
	
	public static void setShop(){
        if (Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("MineSHOP") != null) {
        	shop = new MineSHOP(Main.getPlugin(Main.class));
            Main.economia = Economy.MINESHOP;

        }
	}
	
	public static int getMoney(Player p){
		return shop.getPontos(p.getName());
	}
	
	public static void removeMoney(Player p, int quant){
		shop.setPontos(p.getName(), getMoney(p)- quant);
	}
	
}
