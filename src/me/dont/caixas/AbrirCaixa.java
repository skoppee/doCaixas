package me.dont.caixas;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;


public class AbrirCaixa implements Listener{

	public static void previewBox(Player p, Caixa c){
		
		Inventory inv = Bukkit.createInventory(null, 54, "Conte�do da caixa misteriosa");
		
		int lastIndex = 0;

		
		for (int i = 0; i < 54; i++) {
			if (i < 9 || i == 17 || i == 26 || i == 35 || i == 44 || i == 53 || i % 9 == 0)
				continue;
			if (lastIndex >= c.getOrganizedList().size())
				break;
			inv.setItem(i, c.getOrganizedList().get(lastIndex));
			lastIndex++;
		}
		
		p.openInventory(inv);
	}
	
	@EventHandler
	public void inventoryList(InventoryClickEvent e){
		if (e.getCurrentItem() == null || e.getSlotType().equals(SlotType.OUTSIDE))
			return;
		if (e.getInventory().getName().startsWith("Confirma��o:")){
			String caixa = e.getInventory().getName().replace("Confirma��o: ", "");
			Caixa c = Main.caixas.get(caixa);
			e.setCancelled(true);
			if (e.getSlot()==13){
				previewBox((Player) e.getWhoClicked(), c);
			}
			else if (e.getSlot()==20){
				Player p = (Player) e.getWhoClicked();
				p.closeInventory();
				
				CaixaAbrirEvento evento = new CaixaAbrirEvento(c, p);
				Bukkit.getServer().getPluginManager().callEvent(evento);
				if (evento.isCancelled()){
					return;
				}
				
				if (p.getItemInHand().hasItemMeta() && p.getItemInHand().getItemMeta().hasLore() && p.getItemInHand().getItemMeta().getLore().equals(c.getLores())){
					
					
					
					if (p.getItemInHand().getAmount() > 1){
						p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
					}
					else {
						p.getInventory().remove(p.getItemInHand());
					}
		 	    	ItemStack[] items = c.getSortedList().toArray(new ItemStack[c.getSortedList().size()]);

		 	    	spin(20, p, items, c);
		 	    //	new CrateInventory(27, items, "Caixa: "+c.getIdentifier(), Main.getPlugin(Main.class),c).spin(20, p);;
					
				}
				else {
					p.damage(10);
				}
			}
			else if(e.getSlot()==24){
				e.getWhoClicked().closeInventory();
			}
		}
	}
	
	   private ItemStack randomGlass()
	   {
	     int random = new Random().nextInt(13) + 1;
	     if ((random == 1) || (random == 8)) {
	       random++;
	     }
	     return new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)random);
	   }
	   
	   public void spin(final double seconds, final Player reciever, ItemStack[] contents, Caixa caixa) {
	     Inventory inv = Bukkit.createInventory(null, 27, "Caixa misteriosa");
	     reciever.openInventory(inv);
	     new BukkitRunnable() {
	       double delay = 0;
	       int ticks = 0;
	       boolean done = false;
		     int itemIndex = 0;
	       public void run() {
	         if (done)
	           return;
	         ticks++;
	         delay += 1 / (20 * seconds);
	         if (ticks > delay * 10) {
	           ticks = 0;

	           if (reciever.getOpenInventory().getType() != InventoryType.CHEST)
	           {
	        	   done = true;
	        	   delay = 0.2;
	           }
	           
	          reciever.getWorld().playSound(reciever.getLocation(), Sound.NOTE_STICKS, 10,10);
	           
	           for (int i = 0; i < inv.getSize(); i++){
	        	   if (i >= 9 && i <= 17){
	        	   inv.setItem(i,contents[(i + itemIndex) % contents.length]);
	        	   }
	        	   else {
	        		   if (i != 4 && i !=22){
	        			   inv.setItem(i, randomGlass());
	        		   }
	        		   else {
	        			   inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).setDurability((short) 15).toItemStack());
	        		   }
	        	   }
	        	   
	           }
	           
	           itemIndex++;
	           if (delay >= 0.15) {
	             done = true;
	             new BukkitRunnable() {

	               @Override
	               public void run() {
	            	   inv.clear(9);
	            	   inv.clear(10);
	            	   inv.clear(11);
	            	   inv.clear(12);
	            	   inv.clear(14);
	            	   inv.clear(15);
	            	   inv.clear(16);
	            	   inv.clear(17);

	            	   ItemStack item = inv.getItem(13);
	            	   
	            	   CaixaWinEvent evento = new CaixaWinEvent(caixa, reciever, item);
	            	   if (evento.isCancelled()){
	            		   return;
	            	   }
	            	   
	            	   item = evento.getItem();
	            	   
	            	   inv.setItem(13, evento.getItem());
	            	   
	            	   if (item.hasItemMeta() && item.getItemMeta().hasLore()){
	            		   if (HiddenStringUtils.hasHiddenString(item.getItemMeta().getLore().get(item.getItemMeta().getLore().size()-1))){
	            			   String s = HiddenStringUtils.extractHiddenString(item.getItemMeta().getLore().get(item.getItemMeta().getLore().size()-1));
	            			   
	            			   
	            			   
	            			   if (s.contains("macaquito")){ // raridade e comando
	                			   String raridade = s.split("macaquito")[1];
	            				   if (!Main.raridades.isEmpty()){
	                			   if (Main.raridades.get(raridade) != null){
	                				   Raridade r = Main.raridades.get(raridade);
	                				   if (r.isBroadcast()){
	                					   Bukkit.broadcastMessage(r.getBroadcastMsg().replace("$player", reciever.getName()));
	                				   }
	                				   if (r.isRaio()){
	                					   reciever.getWorld().strikeLightningEffect(reciever.getLocation());
	                				   }
	                			   }
	            				   }
	            				   Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.split("macaquito")[0].replace("$player", reciever.getName()));
	            				   System.out.println("Executando comando: "+s.split("macaquito")[0].replace("$player", reciever.getName()));
	            			   }
	            			   else if (s.contains("$player")){
	            				   Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.replace("$player", reciever.getName()));
	            				   System.out.println("Executando comando: "+s.replace("$player", reciever.getName()));

	            			   }
	            			   else {
	            				   if (!Main.raridades.isEmpty()){
	            				   if (Main.raridades.get(s) != null){
	                				   Raridade r = Main.raridades.get(s);
	                				   if (r.isBroadcast()){
	                					   Bukkit.broadcastMessage(r.getBroadcastMsg().replace("$player", reciever.getName()));
	                				   }
	                				   if (r.isRaio()){
	                					   reciever.getWorld().strikeLightningEffect(reciever.getLocation());
	                				   }
	                			   }
	            				   }
	                    		   reciever.getInventory().addItem(item);

	            			   }
	            		   }
	            		   else {
	                		   reciever.getInventory().addItem(item);
	            		   }
	            	   }
	            	   else {
	            		   reciever.getInventory().addItem(item);
	            	   }
	                 cancel();
	               }
	             }.runTaskLaterAsynchronously(Main.getPlugin(Main.class), 10);
	             new BukkitRunnable() {
					
					@Override
					public void run() {
						if (reciever.getOpenInventory().getType() == InventoryType.CHEST) reciever.closeInventory();
					}
	             }.runTaskLaterAsynchronously(Main.getPlugin(Main.class), 20L*3);
	             cancel();
	           }
	         }
	       }
	     }.runTaskTimer(Main.getPlugin(Main.class), 0, 2);
	   }

	
	@EventHandler
	public void onClick(PlayerInteractEvent e){
		if (e.getAction().name().contains("RIGHT")){
			if (e.hasItem() && e.getItem().hasItemMeta() && e.getItem().getItemMeta().hasLore()) {
				boolean to = false;
				Caixa caixa = null;
				for (Caixa c : Main.caixas.values()){
					if (c.getLores().equals(e.getItem().getItemMeta().getLore())){
						to = true;
						caixa = c;
					}
				}
				if (to){
					Player p = e.getPlayer();
					e.setCancelled(true);
					Inventory inv = Bukkit.createInventory(null, 36, "Confirma��o: "+caixa.getIdentifier());

					inv.setItem(13, caixa.getBox(1));
					
					ArrayList<String> lore1 = new ArrayList<String>();
					lore1.add("�7Voc� ir� ganhar 1 item");
					lore1.add("�7ao abrir essa caixa");
					lore1.add("");
					lore1.add("�7Tem certeza que deseja abr�-la?");
					
					inv.setItem(20, new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 5).setName("�aAceitar").setLore(lore1).toItemStack());
					
					ArrayList<String> lore2 = new ArrayList<String>();
					lore2.add("�7Cancele a abertura");
					lore2.add("�7da caixa misteriosa");
					
					inv.setItem(24, new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 14).setName("�cCancelar").setLore(lore2).toItemStack());
					
					p.openInventory(inv);
				}
			}
		}
		if (e.getAction().name().contains("LEFT")){
			if (e.hasItem() && e.getItem().hasItemMeta() && e.getItem().getItemMeta().hasLore()) {
				boolean to = false;
				Caixa caixa = null;
				for (Caixa c : Main.caixas.values()){
					if (c.getLores().equals(e.getItem().getItemMeta().getLore())){
						to = true;
						caixa = c;
					}
				}
				if (to){
					previewBox(e.getPlayer(), caixa);
				}
			}
		}
			
	}
	
	
	
}
