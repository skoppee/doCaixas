package me.dont.caixas;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;import com.mysql.jdbc.ReflectiveStatementInterceptorAdapter;

/**
 * Created by Zombie_Striker.
 * https://bukkit.org/threads/csgo-crates-create-inventorys
 * -the-cycle-through-items.431949
 */

public class CrateInventory {

   private Inventory inv;
   private Plugin plugin;
   private ItemStack[] contents;
   private Caixa caixa;
   private InventoryClickHandler helper;

   private int itemIndex = 0;

   public CrateInventory(int size, ItemStack[] contents, String name,
       Plugin main, Caixa c) {
     inv = Bukkit.createInventory(null, size, name);
     this.plugin = main;
     this.contents = contents;
     helper = new InventoryClickHandler(main, name);
     caixa = c;
     shuffle();
   }

   /**
    * Shuffles the existing inventory.
    *
    * Note that this does not shuffle the order of the items. This only shuffles the starting index.
    */
   public void shuffle() {
     int startingIndex = ThreadLocalRandom.current().nextInt(
         this.contents.length);
     for (int index = 0; index < startingIndex; index++) {
       for (int itemstacks = 0; itemstacks < inv.getSize(); itemstacks++) {
         inv.setItem(itemstacks, contents[(itemstacks + itemIndex)
             % contents.length]);
       }
       itemIndex++;
     }
   }

   /**
    * Returns the inventory click manager. There should be no reason why you
    * need it unless you want to unregister the class (meaning players can pick
    * up items)
    *
    * @return
    */
   public InventoryClickHandler getInventoryClickManager() {
     return helper;
   }

   /**
    * Sets the contents for the crate.
    *
    * @param contents
    */
   public void setContents(ItemStack[] contents) {
     this.contents = contents;
   }

   /**
    * Returns the contents of the crate
    *
    * @return The contents of the crate
    */
   public ItemStack[] getContents() {
     return this.contents;
   }

   /**
    * Returns the inventory instance
    *
    * @return The inventory instance.
    */
   public Inventory getInventory() {
     return inv;
   }

   /**
    * This spins the inventory. Call this to play the animation and give a
    * random item.
    *
    * @param seconds
    *  the amount of second you want this to spin.
    * @param reciever
    *  player who should recieve the item.
    */
   
   private ItemStack randomGlass()
   {
     int random = new Random().nextInt(13) + 1;
     if ((random == 1) || (random == 8)) {
       random++;
     }
     return new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)random);
   }
   
   public void spin(final double seconds, final Player reciever) {
     reciever.openInventory(this.inv);
     new BukkitRunnable() {
       double delay = 0;
       int ticks = 0;
       boolean done = false;

       public void run() {
         if (done)
           return;
         ticks++;
         delay += 1 / (20 * seconds);
         if (ticks > delay * 10) {
           ticks = 0;

           if (reciever.getOpenInventory().getType() != InventoryType.CHEST)
           {
        	   done = true;
        	   delay = 0.2;
           }
           
          reciever.getWorld().playSound(reciever.getLocation(), Sound.NOTE_STICKS, 10,10);
           
           for (int itemstacks = 0; itemstacks < inv.getSize(); itemstacks++){
        	   if (itemstacks >= 9 && itemstacks <= 17){
        	   inv.setItem(itemstacks,contents[(itemstacks + itemIndex) % contents.length]);
        	   }
        	   else {
        		   if (itemstacks != 4 && itemstacks !=22){
        			   inv.setItem(itemstacks, randomGlass());
        		   }
        		   else {
        			   inv.setItem(itemstacks, new ItemBuilder(Material.STAINED_GLASS_PANE).setDurability((short) 15).toItemStack());
        		   }
        	   }
        	   
           }
           
           itemIndex++;
           if (delay >= 0.15) {
             done = true;
             new BukkitRunnable() {

               @Override
               public void run() {
            	   inv.clear(9);
            	   inv.clear(10);
            	   inv.clear(11);
            	   inv.clear(12);
            	   inv.clear(14);
            	   inv.clear(15);
            	   inv.clear(16);
            	   inv.clear(17);

            	   ItemStack item = inv.getItem(13);
            	   
            	   CaixaWinEvent evento = new CaixaWinEvent(caixa, reciever, item);
            	   if (evento.isCancelled()){
            		   return;
            	   }
            	   
            	   item = evento.getItem();
            	   
            	   inv.setItem(13, evento.getItem());
            	   
            	   if (item.hasItemMeta() && item.getItemMeta().hasLore()){
            		   if (HiddenStringUtils.hasHiddenString(item.getItemMeta().getLore().get(item.getItemMeta().getLore().size()-1))){
            			   String s = HiddenStringUtils.extractHiddenString(item.getItemMeta().getLore().get(item.getItemMeta().getLore().size()-1));
            			   
            			   
            			   
            			   if (s.contains("macaquito")){ // raridade e comando
                			   String raridade = s.split("macaquito")[1];
            				   if (!Main.raridades.isEmpty()){
                			   if (Main.raridades.get(raridade) != null){
                				   Raridade r = Main.raridades.get(raridade);
                				   if (r.isBroadcast()){
                					   Bukkit.broadcastMessage(r.getBroadcastMsg().replace("$player", reciever.getName()));
                				   }
                				   if (r.isRaio()){
                					   reciever.getWorld().strikeLightningEffect(reciever.getLocation());
                				   }
                			   }
            				   }
            				   Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.split("macaquito")[0].replace("$player", reciever.getName()));
            				   System.out.println("Executando comando: "+s.split("macaquito")[0].replace("$player", reciever.getName()));
            			   }
            			   else if (s.contains("$player")){
            				   Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.replace("$player", reciever.getName()));
            				   System.out.println("Executando comando: "+s.replace("$player", reciever.getName()));

            			   }
            			   else {
            				   if (!Main.raridades.isEmpty()){
            				   if (Main.raridades.get(s) != null){
                				   Raridade r = Main.raridades.get(s);
                				   if (r.isBroadcast()){
                					   Bukkit.broadcastMessage(r.getBroadcastMsg().replace("$player", reciever.getName()));
                				   }
                				   if (r.isRaio()){
                					   reciever.getWorld().strikeLightningEffect(reciever.getLocation());
                				   }
                			   }
            				   }
                    		   reciever.getInventory().addItem(item);

            			   }
            		   }
            		   else {
                		   reciever.getInventory().addItem(item);
            		   }
            	   }
            	   else {
            		   reciever.getInventory().addItem(item);
            	   }
                 cancel();
               }
             }.runTaskLaterAsynchronously(plugin, 10);
             new BukkitRunnable() {
				
				@Override
				public void run() {
					if (reciever.getOpenInventory().getType() == InventoryType.CHEST) reciever.closeInventory();
				}
             }.runTaskLaterAsynchronously(plugin, 20L*3);
             cancel();
           }
         }
       }
     }.runTaskTimer(plugin, 0, 2);
   }
}


class InventoryClickHandler implements Listener {
   private String name = null;

   public InventoryClickHandler(Plugin plugin, String inventoryName) {
     Bukkit.getPluginManager().registerEvents(this, plugin);
     this.name = inventoryName;
   }

   @EventHandler
   public void onClick(InventoryClickEvent e) {
     if (e.getInventory().getTitle().equals(name)) {
       e.setCancelled(true);
     }
   }

   public void changeName(String name) {
     this.name = name;
   }
}