package me.dont.caixas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Caixa {

	private String caixa;
	private String identifier;
	private boolean glow;
	private Material id;
	private int preco;
	private int slot;
	private List<String> lores;
	private List<String> lorespreco;

	private HashMap<HashMap<Tipo, ItemStack>, Integer> items;
	private List<ItemStack> sortedList; // Usado quando a caixa abre
	private List<ItemStack> organizedList; // Usado na preview da caixa

	public Caixa(String real,String nome, Material item, HashMap<HashMap<Tipo, ItemStack>, Integer> loot, List<String> lore, boolean brilhar, int preco, int slot, List<String> lorecompreco){
		caixa = ChatColor.translateAlternateColorCodes('&', nome);
		identifier = real;
		id = item;
		items = loot;
		lorespreco = lorecompreco;
		lores = lore;
		this.slot = slot;
		glow = brilhar;
		this.preco = preco;
		
		List<ItemStack> toReturn2 = new ArrayList();
		for (Entry<HashMap<Tipo, ItemStack>, Integer> a : items.entrySet()){
				for (ItemStack stack : a.getKey().values()){
					ItemStack itemstack = stack.clone();
					List<String> loren = new ArrayList<>();
					if (itemstack.hasItemMeta() && itemstack.getItemMeta().hasLore()){
						itemstack.getItemMeta().getLore().clear();
						List<String> loresa = itemstack.getItemMeta().getLore();
						for (String s : loresa){
							if (!HiddenStringUtils.hasHiddenString(s)){
								loren.add(s);
							}

						}

					}
					loren.add("�6Chance: �f"+a.getValue()+"%");
					toReturn2.add(new ItemBuilder(itemstack).setLore(loren).toItemStack());
				}
		}
		HashMap<ItemStack, Integer> top = new HashMap<>();
		for (ItemStack s : toReturn2){
			String replace = s.getItemMeta().getLore().get(s.getItemMeta().getLore().size()-1);
			replace = replace.replace("6Chance:", "").replace("%", "");
			replace = ChatColor.stripColor(replace);
			replace = replace.replace("�", "");
			replace = replace.replace(" ", "");
			top.put(s, Integer.valueOf(replace));
		}
		toReturn2.clear();

		for (Entry<ItemStack, Integer> a : Main.entriesSortedByValues(top)){
			toReturn2.add(a.getKey());
		}
		organizedList = toReturn2;
		
		
		List<ItemStack> toReturn = new ArrayList();
		for (Entry<HashMap<Tipo, ItemStack>, Integer> a : items.entrySet()){
			for (int i = 0; i <= a.getValue(); i++){
				for (ItemStack stack : a.getKey().values()){
					toReturn.add(stack);
				}
			}
		}
		sortedList = toReturn;
		
	}

	public int getSlot(){
		return slot;
	}
	
	public List<String> getLores(){
		return lores;
	}
	
	public List<String> getLoresPreco(){
		return lorespreco;
	}
	
	public int getPreco(){
		return preco;
	}
	
	public ItemStack getBox(Integer quantia){
		return new ItemBuilder(id,quantia).setName(caixa).setLore(lores).setGlow(glow).toItemStack();
	}
	
	public ItemStack getBox(){
		return new ItemBuilder(id).setName(caixa).setLore(lores).setGlow(glow).toItemStack();
	}
	
	public ItemStack getBoxToMenu(){
		return new ItemBuilder(id).setName(caixa).setLore(lorespreco).setGlow(glow).toItemStack();
	}
	
	
	public List<ItemStack> getOrganizedList(){
		return organizedList;
	}
	
	public List<ItemStack> getSortedList(){
		long seed = System.nanoTime();
	    	Collections.shuffle(sortedList, new Random(seed));
		return sortedList;
	}
	
	public String getNome(){
		return caixa;
	}
	
	public String getIdentifier(){
		return identifier;
	}
	
	public void setNome(String s){
		caixa = s;
	}
	
	public Material getId(){
		return id;
	}
	
	public HashMap<HashMap<Tipo, ItemStack>, Integer> getLoot(){
		return items;
	}
	
	public void setId(Material s){
		id = s;
	}
	
	public String toString(){
		return "{caixa="+getNome()+",identifier="+getIdentifier()+",items="+getSortedList().size()+"}";
	}
	
	enum Tipo {
		ITEM,
		COMANDO;
	}
	
}
